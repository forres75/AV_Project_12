# Final_Project



## Deadlines

December 1st, 3pm:

* Push a report outline called Report_Outline with a couple bullets for each of the above six report sections.  By this point you should have selected your risk area and what you will demonstrate.  It is okay to modify these later.
* (2 points)



December 11th, 3pm:

* Push full report called Report to Gitlab
* (25 points)



December 14th, 10am:

In-person teams:

* Push your ROS package to Gitlab.
Do a live presentation during final exam period.  Each team member must present a portion of it.  Introduce yourself when you start talking.
* (10 points presentation, 7 points code, 6 points demonstration)


!
