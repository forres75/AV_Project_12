import rclpy
from rclpy.node import Node
from rclpy.duration import Duration
import time
import os
import scipy.integrate as integrate
import scipy.special as special
from scipy.special import expit 
from sensor_msgs.msg import Imu, JointState
import sensor_msgs
import math
from geometry_msgs.msg import Twist
#from .slip_detect import SlipDetect

class speed_monitor(Node):
    def __init__(self):
        super().__init__('speed_monitor')     
        #self.subscription1 = self.create_subscription(
        #    Imu,
        #    '/imu',
        #    self.speed_cb,
        #    1) 
        #self.get_logger().info("subscribing to Imu")
        self.subscription = self.create_subscription(
            Twist,
            '/cmd_vel',
            self.accel_cb,
            1) 
        self.x1 = 0
        self.y1 = 0
        self.x2 = 0
        self.y2 = 0
        self.count = 0
    def accel_cb(self, Twist):
        times = time()
        if (self.count == 2):
            self.count = 0
        if (self.count == 0):
            self.x1 = Twist.linear.x
            self.y1 = Twist.linear.y
        if (self.count == 1):
            self.x2 = Twist.linear.x
            self.y2 = Twist.linear.y
        change_in_x = (self.x2-self.x1)
        change_in_y = (self.y2-self.y1)
        self.count +=1
        return change_in_x, change_in_y



def main(args=None):
    rclpy.init(args=args)


    node = speed_monitor()  
    try:
        rclpy.spin(node)       
    except SystemExit:
        node.destroy_node()
        rclpy.shutdown()
    except KeyboardInterrupt:
        pass
    