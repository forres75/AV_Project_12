import rclpy
from rclpy.node import Node
from rclpy.duration import Duration
import time
import os
import scipy.integrate as integrate
import scipy.special as special
from scipy.special import expit 
from sensor_msgs.msg import Imu, JointState
import sensor_msgs
import math
from geometry_msgs.msg import Twist, PoseStamped, Vector3
from visualization_msgs.msg import MarkerArray
import numpy as np
#from .obstacles import Obstacles



class SlipDetect(Node):
    def __init__(self):
        super().__init__('SlipDetect')  
        #x = Imu()   
        #print(x.linear_acceleration) 
        #self.obs = Obstacles()  
        self.subscription1 = self.create_subscription(
            Imu,
            '/imu',
            self.speed_cb,
            1) 
        self.get_logger().info("subscribing to Imu")
        self.subscription = self.create_subscription(
            Twist,
            '/cmd_vel',
            self.accel_cb,
            1)
        self.subscription2 = self.create_subscription(
            MarkerArray,
            'obstacles',
            self.accelCheck_cb,
            1)
        self.publisher = self.create_publisher(
            Twist,
            '/cmd_vel',
            1)
        
        self.speedx = 0
        self.speedy = 0
        self.timestart = 0
        self.timeend = 0
        self.prevaccx = 0
        self.prevaccy = 0
        self.velocity = 0
        self.count = 0
        self.x = 0
        self.y = 0
        self.x1 = 0
        self.y1 = 0
        self.x1 = 0
        self.y1 = 0
        self.x2 = 0
        self.y2 = 0
        self.x3 = 0
        self.y3 = 0
        self.count1 = 0
        self.change_in_x = 0
        self.change_in_y = 0
        self.closest = {'x': 0, 'y': 0, 'time': 0}
        self.closest1 = {'x': 0, 'y': 0, 'time': 0}
        self.closest_speed = {'x': 0, 'y': 0, 'time': 0}
        self.closest_speed1 = {'x': 0, 'y': 0, 'time': 0}
        self.closest_speed_current = {'x': 0, 'y': 0, 'time': 0}
        self.closest_count = 0
        self.max = {'x': 0, 'y': 0}
        self.min = {'x': 0, 'y': 0}
        self.xerror = 0.9
        self.yerror = 0.5
        self.accel_tracker_x = []
        self.accel_tracker_y = []
        self.blob = 0
        self.current_speed_x = 0
        self.current_speed_y = 0
        self.current_speed_angular = 0
        self.startspeed = Twist( linear=Vector3(x=0.2), angular=Vector3(z=0.1))
        self.alternate = 1
        
    def accelCheck_cb(self, msg):
        if (self.closest_count == 2):
            self.closest_count = 0
        points = np.array(msg.markers)
        #print(points[0])
        closest = np.Inf
        closestindex = 0
        for i in range(len(points)):
            x = points[i].pose.position.x
            y = points[i].pose.position.y
            ranges = math.sqrt(x**2 + y**2)
            if ranges < closest:
                closest = ranges
                closestindex = i
        try:
            x = points[closestindex].pose.position.x
            y = points[closestindex].pose.position.y
            nano = points[closestindex].header.stamp.nanosec
            sec = points[closestindex].header.stamp.sec
            ranges = math.sqrt(x**2 + y**2)
        except:
            x = 0.00
            y = 0.00
            ranges = 1.00
            nano = 0.00
            sec = self.alternate
            self.alternate = self.alternate*(-1)
    
        if (self.closest_count == 0):
            self.closest['x'] = x/ranges
            self.closest['y'] = y/ranges
            self.closest['time'] = sec + nano*(0.000000001) # behind speed_cb by 0.061 sec
        if (self.closest_count == 1):
            self.closest1['x'] = x/ranges
            self.closest1['y'] = y/ranges
            self.closest1['time'] = sec + nano*(0.000000001)
        if (abs(self.closest['x']-self.closest1['x']) > self.xerror):
            self.closest_speed_current['x'] = abs(self.closest['x']-self.closest1['x'])/abs(self.closest['time']-self.closest1['time'])
            self.closest_speed_current['time'] = sec + nano*(0.000000001)
        if (abs(self.closest['y']-self.closest1['y']) > self.xerror):
            self.closest_speed_current['y'] = abs(self.closest['y']-self.closest1['y'])/abs(self.closest['time']-self.closest1['time'])
            self.closest_speed_current['time'] = sec + nano*(0.000000001)
        if (self.closest_count == 0):
            self.closest_speed['x'] = self.closest_speed_current['x']
            self.closest_speed['y'] = self.closest_speed_current['y']
            self.closest_speed['time'] =  sec + nano*(0.000000001)
        if (self.closest_count == 1):
            self.closest_speed1['x'] = self.closest_speed_current['x']
            self.closest_speed1['y'] = self.closest_speed_current['y']
            self.closest_speed1['time'] =  sec + nano*(0.000000001)
        
        if (self.blob == 1):
            accelx = ((2*abs(self.closest_speed['x']-self.closest_speed1['x']))/(2*abs(self.closest_speed['time']-self.closest_speed1['time'])))
            accely = ((2*abs(self.closest_speed['y']-self.closest_speed1['y']))/(2*abs(self.closest_speed['time']-self.closest_speed1['time'])))
            self.add_acceleration(accelx, accely)
        #print(self.closest_speed)
        self.closest_count += 1
    def accel_cb(self, Twist):
        print(Twist.linear)
        self.current_speed_x = Twist.linear.x
        self.current_speed_y = Twist.linear.y
        self.current_speed_angular = Twist.angular
        #print(self.current_speed_angular)
        if (self.count1 == 2):
            self.count1 = 0
        if (self.count1 == 0):
            self.x1 = Twist.linear.x
            self.y1 = Twist.linear.y
        if (self.count1 == 1):
            self.x2 = Twist.linear.x
            self.y2 = Twist.linear.y
        change_in_x = (self.x2-self.x1)
        change_in_y = (self.y2-self.y1)
        self.count1 +=1
        self.change_in_x = change_in_x
        self.change_in_y = change_in_y
        if ((self.change_in_x != 0) or (self.change_in_y != 0)):
            self.blob = 1
            
            

    def speed_cb( self, Imu ):
        timenano = Imu.header.stamp.nanosec
        time = Imu.header.stamp.sec
        #print(self.closest['time'])
        time = timenano*(0.000000001) +time
        #print(time)
        angle = Imu.orientation.w # goes from -1 to 1. one full rotation is from -1 to 0 or 0 to 1. 1 or -1 is facing up on gazebo world.
        #print(Imu.angular_velocity)
        errorx = 0.09
        errory = 0.09
        slip_error = 0.1
        accel = Imu.linear_acceleration
        #print(accel)
        rate1x = 0
        rate2x = 0
        rate1y = 0
        rate2y = 0
        rate3x = 0
        rate3y = 0
        if (self.count == 0):
            self.timestart = time
            self.prevaccx = accel.x
            self.prevaccy = accel.y
            self.count = 1
        if (self.count == 4):
            self.count = 1
        elif (self.count == 1):
            self.x = accel.x
            self.y = accel.y
            rate1x = self.change_in_x
            rate1y = self.change_in_y
        elif (self.count == 2):
            self.x1 = accel.x
            self.y1 = accel.y
            rate2x = self.change_in_x
            rate2y = self.change_in_y
        elif (self.count == 3):
            self.x2 = accel.x
            self.y2 = accel.y
            rate3x = self.change_in_x
            rate3y = self.change_in_y
        accel.x = (self.x+self.x1+self.x2)/3
        accel.y = (self.y+self.y1+self.y2)/3
        deltax = (rate1x+rate2x+rate3x)/3
        deltay = (rate1y+rate2y+rate3y)/3
        


        #print(time)
        if ((accel.x < (self.prevaccx - errorx)) or (accel.x > (self.prevaccx + errorx))):
            #print(accel.x)
            self.prevaccx = accel.x
            self.timeend = time
            try:
                speed_rate_x = deltax/(self.timeend-self.timestart)
            except:
                speed_rate_x = 0
            #speedx = self.speedx + accel.x*(self.timeend-self.timestart)
            #self.speedx = speedx
            #if ((accel.x < (speed_rate_x - slip_error)) or (accel.x > (speed_rate_x + slip_error))):
            if (speed_rate_x == 0):
                if (self.blob == 0):
                    print("Slip in x")
                    speed = abs(float(self.current_speed_x)-0.05)
                    if (speed <= 0.001):
                        speed = 0.00
                    #angle = abs(float(self.current_speed_angular)-0.005)
                    motion = Twist( linear=Vector3(x=float(speed)), angular=Vector3(z=0.0))
                    self.publisher.publish (motion )
            
            #print(self.speedx)
        if ((accel.y < (self.prevaccy - errory)) or (accel.y > (self.prevaccy + errory))):
            #print(accel.y)
            self.prevaccy = accel.y
            self.timeend = time
            #speedy = self.speedy + accel.x*(self.timeend-self.timestart)
            #self.speedy = speedy
            try:
                speed_rate_y = deltay/(self.timeend-self.timestart)
            except:
                speed_rate_y = 0
            #if ((accel.y < (speed_rate_y - slip_error)) or (accel.y > (speed_rate_y + slip_error))):
            if (speed_rate_y == 0):
                if (self.blob == 0):
                    print("Slip in y")
                    speed = abs(float(self.current_speed_x)-0.05)
                    if (speed <= 0.001):
                        speed = 0.0
                    #angle = abs(float(self.current_speed_angular)-0.005)
                    motion = Twist( linear=Vector3(x=float(speed)), angular=Vector3(z=0.0))
                    self.publisher.publish (motion )
            #print(self.speedy)
        #self.velocity = math.sqrt(self.speedx**2 + self.speedy**2)
        #print(self.velocity)
        self.timestart=time
        
        if (self.count == 1):
            try:
                motion = self.startspeed
                self.publisher.publish (motion )
                #print("Linear velocity is 0.1. Angular velocity is 0.001")
                self.startspeed = 0
            except:
                pass
        self.count +=1


    def add_acceleration(self, accelx, accely):
        if (accelx >= 2 or accely >= 2):
            pass
            #self.accel_tracker_x.append(accelx)
            #self.accel_tracker_y.append(accely)

        else:    
            #self.accel_tracker_x = []
            #self.accel_tracker_y = []
            self.blob = 0

		
         
        
        
        
       
       

def main(args=None):
    rclpy.init(args=args)


    node = SlipDetect()  
    try:
        rclpy.spin(node)       
    except SystemExit:
        node.destroy_node()
        rclpy.shutdown()
    except KeyboardInterrupt:
        pass