#!/usr/bin/env python
import rclpy
from rclpy.node import Node
from rclpy.duration import Duration
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Pose, Point, Quaternion, PoseStamped
from visualization_msgs.msg import Marker, MarkerArray
import numpy as np
import math

class Obstacles(Node):
    def __init__(self, delta_r=0.25):
        super().__init__('bus_monitor')             
        self.delta_r = delta_r

        self.publisher = self.create_publisher(MarkerArray, 'obstacles', 1)    
        self.subscription = self.create_subscription(LaserScan, 'scan', self.lidar_centroids, 1)
        self.subscription

    def lidar_centroids(self, msg):       
        ranges = np.array(msg.ranges)  # Convert to Numpy array for vector operations
        if len(ranges)==0:
            return
        angles = np.arange(len(ranges)) * msg.angle_increment + msg.angle_min # Angle of each ray
        x = ranges * np.cos(angles) # vector arithmatic is much faster than iterating
        y = ranges * np.sin(angles)

        # <...> Here is where you find the obstacles
        # Calculate xcen and ycen: two numpy arrays with coordinates of object centers
        
        rangeobj = []
        xobj = []
        yobj = []
        inc = 0
        numinobj = []
        inc2 = 0
        for i in range(len(x)-1):
            if (math.sqrt(x[i]**2+y[i]**2) != 0) & (math.sqrt(x[i]**2+y[i]**2) != np.Inf):
                if (abs(math.sqrt(x[i]**2+y[i]**2)-math.sqrt(x[i+1]**2+y[i+1]**2)) < 0.25):
                    try:
                        xobj[inc] += x[i]
                    except:
                        xobj = np.append(xobj, x[i])
                    try:
                        yobj[inc] += y[i]
                    except:
                        yobj = np.append(yobj, y[i])
                    inc2 = inc2 + 1
                    if i == 358:
                        xobj[0] += xobj[inc]
                        xobj = np.delete(xobj, inc)
                        yobj[0] += yobj[inc]
                        yobj = np.delete(yobj, inc)
                        numinobj[0] += inc2
                elif (abs(math.sqrt(x[i]**2+y[i]**2)-math.sqrt(x[i-1]**2+y[i-1]**2)) < 0.25) & (i < 359):
                    try:
                        xobj[inc] += x[i]
                    except:
                        xobj = np.append(xobj, x[i])
                    try:
                        yobj[inc] += y[i]
                    except:
                        yobj = np.append(yobj, y[i])
                    numinobj = np.append(numinobj, inc2)
                    inc2 = 0
                    inc = inc + 1
        xcenter = []
        ycenter = []
        for i in range(len(xobj)):
            xcenter = np.append(xcenter, xobj[i]/numinobj[i])
            ycenter = np.append(ycenter, yobj[i]/numinobj[i])
        
        xcen = xcenter
        ycen = ycenter

        # Convert to list of point vectors
        points = np.column_stack( (xcen, ycen, np.zeros_like(xcen)) ).tolist()

        ids = np.arange(len(points)).astype(int)

        self.pub_centroids(points, ids, msg.header)

      
    def pub_centroids(self, points, ids, header):

        ma = MarkerArray()

        for id, p in zip(ids, points):
            mark = Marker()            
            mark.header = header
            mark.id = id.item()
            mark.type = Marker.SPHERE
            mark.pose = Pose(position=Point(x=p[0],y=p[1],z=p[2]), orientation=Quaternion(x=0.,y=0.,z=0.,w=1.))
            mark.scale.x = 0.25
            mark.scale.y = 0.25
            mark.scale.z = 0.25
            mark.color.a = 0.75
            mark.color.r = 0.25
            mark.color.g = 1.
            mark.color.b = 0.25
            mark.lifetime = Duration(seconds=0.4).to_msg()
            ma.markers.append(mark)

        self.publisher.publish( ma )

def main(args=None):

    rclpy.init(args=args)

    node = Obstacles()
    rclpy.spin(node) 


